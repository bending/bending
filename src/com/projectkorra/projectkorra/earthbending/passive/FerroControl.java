package com.projectkorra.projectkorra.earthbending.passive;

import com.projectkorra.projectkorra.GeneralMethods;
import com.projectkorra.projectkorra.ability.MetalAbility;
import com.projectkorra.projectkorra.ability.PassiveAbility;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Player;

import java.util.HashSet;

public class FerroControl extends MetalAbility implements PassiveAbility {

    private Block block;

    public FerroControl(final Player player) {
        super(player);
        start();
    }

    @Override
    public void progress() {
        if (!this.player.isSneaking() || !this.bPlayer.canUsePassive(this) || !this.bPlayer.canBendPassive(this)) {
            remove();
            return;
        }

        boolean used = false, tDoor = false, open = false;
        //TODO FIX
        /*24.07 05:11:51 [Server] WARN java.lang.IllegalStateException: Start block missed in BlockIterator
        24.07 05:11:51 [Server] WARN at org.bukkit.util.BlockIterator.<init>(BlockIterator.java:169)
        24.07 05:11:51 [Server] WARN at org.bukkit.util.BlockIterator.<init>(BlockIterator.java:188)
        24.07 05:11:51 [Server] WARN at org.bukkit.util.BlockIterator.<init>(BlockIterator.java:223)
        24.07 05:11:51 [Server] WARN at org.bukkit.craftbukkit.v1_12_R1.entity.CraftLivingEntity.getLineOfSight(CraftLivingEntity.java:108)
        24.07 05:11:51 [Server] WARN at org.bukkit.craftbukkit.v1_12_R1.entity.CraftLivingEntity.getTargetBlock(CraftLivingEntity.java:134)
        24.07 05:11:51 [Server] WARN at com.projectkorra.projectkorra.earthbending.passive.FerroControl.progress(FerroControl.java:32)
        24.07 05:11:51 [Server] WARN at com.projectkorra.projectkorra.ability.CoreAbility.progressAll(CoreAbility.java:145)
        24.07 05:11:51 [Server] WARN at com.projectkorra.projectkorra.BendingManager.run(BendingManager.java:57)
        24.07 05:11:51 [Server] WARN at org.bukkit.craftbukkit.v1_12_R1.scheduler.CraftTask.run(CraftTask.java:73)
        24.07 05:11:51 [Server] WARN at org.bukkit.craftbukkit.v1_12_R1.scheduler.CraftScheduler.mainThreadHeartbeat(CraftScheduler.java:426)
        24.07 05:11:51 [Server] WARN at net.minecraft.server.MinecraftServer.func_71190_q(MinecraftServer.java:853)
        24.07 05:11:51 [Server] WARN at net.minecraft.server.dedicated.DedicatedServer.func_71190_q(DedicatedServer.java:461)
        24.07 05:11:51 [Server] WARN at net.minecraft.server.MinecraftServer.func_71217_p(MinecraftServer.java:803)
        24.07 05:11:51 [Server] WARN at net.minecraft.server.MinecraftServer.run(MinecraftServer.java:650)
        24.07 05:11:51 [Server] WARN at java.lang.Thread.run(Thread.java:748)*/
        this.block = this.player.getTargetBlock((HashSet<Material>) null, 5);

        if (this.block != null) {
            if (this.block.getType() == Material.IRON_DOOR_BLOCK && !GeneralMethods.isRegionProtectedFromBuild(this.player, this.block.getLocation())) {
                if (this.block.getData() >= 8) {
                    this.block = this.block.getRelative(BlockFace.DOWN);
                }

                this.block.setData((byte) ((this.block.getData() & 0x4) == 0x4 ? (this.block.getData() & ~0x4) : (this.block.getData() | 0x4)));
                open = (this.block.getData() & 0x4) == 0x4;
                used = true;
            } else if (this.block.getType() == Material.IRON_TRAPDOOR && !GeneralMethods.isRegionProtectedFromBuild(this.player, this.block.getLocation())) {
                this.block.setData((byte) ((this.block.getData() & 0x4) == 0x4 ? (this.block.getData() & ~0x4) : (this.block.getData() | 0x4)));
                open = (this.block.getData() & 0x4) == 0x4;
                used = true;
                tDoor = true;
            }

        }

        if (used) {
            final String sound = "BLOCK_IRON_" + (tDoor ? "TRAP" : "") + "DOOR_" + (open ? "OPEN" : "CLOSE");
            this.block.getWorld().playSound(this.block.getLocation(), Sound.valueOf(sound), 0.5f, 0);
            this.bPlayer.addCooldown(this, 200);
        }
        remove();
    }

    @Override
    public boolean isSneakAbility() {
        return true;
    }

    @Override
    public boolean isHarmlessAbility() {
        return true;
    }

    @Override
    public long getCooldown() {
        return 0;
    }

    @Override
    public String getName() {
        return "FerroControl";
    }

    @Override
    public Location getLocation() {
        return this.block != null ? this.block.getLocation() : null;
    }

    @Override
    public boolean isInstantiable() {
        return false;
    }

    @Override
    public boolean isProgressable() {
        return true;
    }
}
