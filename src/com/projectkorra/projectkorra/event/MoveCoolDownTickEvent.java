package com.projectkorra.projectkorra.event;

import com.projectkorra.projectkorra.BendingPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class MoveCoolDownTickEvent extends Event implements Cancellable {
    private static final HandlerList HANDLERS = new HandlerList();

    private final BendingPlayer player;
    private boolean cancelled = false;

    public MoveCoolDownTickEvent(final BendingPlayer player) {
        this.player = player;
    }

    public static HandlerList getHandlerList() {
        return HANDLERS;
    }

    public BendingPlayer getBendingPlayer() {
        return this.player;
    }

    public Player getPlayer() {
        return this.player.getPlayer();
    }

    @Override
    public HandlerList getHandlers() {
        return HANDLERS;
    }

    @Override
    public boolean isCancelled() {
        return this.cancelled;
    }

    @Override
    public void setCancelled(final boolean cancelled) {
        this.cancelled = cancelled;
    }

}
