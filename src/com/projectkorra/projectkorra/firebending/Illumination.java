package com.projectkorra.projectkorra.firebending;

import com.projectkorra.projectkorra.Element;
import com.projectkorra.projectkorra.ability.FireAbility;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Player;

public class Illumination extends FireAbility {

    private int x;
    private int y;
    private int z;
    private Location loc;

    public Illumination(final Player player) {
        super(player);

        if (this.bPlayer.isOnCooldown(this)) {
            return;
        }

        this.start();
    }

    @Override
    public void progress() {
        if (!this.bPlayer.canBendIgnoreBindsCooldowns(this)) {
            this.remove();
            return;
        }

        if (!this.bPlayer.isIlluminating()) {
            this.remove();
            return;
        }

        if (this.bPlayer.hasElement(Element.EARTH) && this.bPlayer.isTremorSensing()) {
            this.remove();
            return;
        }

        final Block block = this.player.getLocation().getBlock();
        final Block blockDown = block.getRelative(BlockFace.DOWN, 1);
        Location loc = block.getLocation();
        boolean set = this.x == loc.getBlockX() && this.y == loc.getBlockY() && this.z == loc.getBlockZ();

        if (!set) {
            this.update();
            if (block.isEmpty() && blockDown.getType().isSolid()) {
                this.set(loc, Material.TORCH, (byte) 0);
            }
        }
    }

    @Override
    public void remove() {
        this.update();
        super.remove();
    }

    public void update() {
        if (this.loc != null) {
            Block block = this.loc.getBlock();
            Material mat = block.getType();
            byte b = block.getData();
            this.set(this.loc, mat, b);
        }
    }

    private void set(Location loc, Material mat, byte b) {
        this.x = loc.getBlockX();
        this.y = loc.getBlockY();
        this.z = loc.getBlockZ();
        this.loc = loc;
        player.sendBlockChange(loc, mat, b);
    }

    @Override
    public String getName() {
        return "Illumination";
    }

    @Override
    public Location getLocation() {
        return loc;
    }

    @Override
    public long getCooldown() {
        return 0;
    }

    @Override
    public boolean isSneakAbility() {
        return true;
    }

    @Override
    public boolean isHarmlessAbility() {
        return true;
    }

}
