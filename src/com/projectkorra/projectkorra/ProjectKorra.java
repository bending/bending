package com.projectkorra.projectkorra;

import com.projectkorra.projectkorra.ability.CoreAbility;
import com.projectkorra.projectkorra.ability.util.*;
import com.projectkorra.projectkorra.airbending.util.AirbendingManager;
import com.projectkorra.projectkorra.chiblocking.util.ChiblockingManager;
import com.projectkorra.projectkorra.command.Commands;
import com.projectkorra.projectkorra.configuration.ConfigManager;
import com.projectkorra.projectkorra.earthbending.util.EarthbendingManager;
import com.projectkorra.projectkorra.firebending.util.FirebendingManager;
import com.projectkorra.projectkorra.object.Preset;
import com.projectkorra.projectkorra.storage.DBConnection;
import com.projectkorra.projectkorra.util.*;
import com.projectkorra.projectkorra.waterbending.util.WaterbendingManager;
import com.sk89q.worldguard.bukkit.WorldGuardPlugin;
import com.sk89q.worldguard.protection.flags.StateFlag;
import com.sk89q.worldguard.protection.flags.registry.FlagRegistry;
import org.bukkit.Bukkit;
import org.bukkit.Statistic;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.logging.Logger;

public class ProjectKorra extends JavaPlugin {

    public static StateFlag BENDING = new StateFlag("bending", false);
    public static StateFlag AVATAR_BENDING = new StateFlag("avatar-bending", true);
    public static ProjectKorra plugin;
    public static Logger log;
    public static CollisionManager collisionManager;
    public static CollisionInitializer collisionInitializer;
    public static long time_step = 1;

    public static CollisionManager getCollisionManager() {
        return collisionManager;
    }

    public static void setCollisionManager(final CollisionManager collisionManager) {
        ProjectKorra.collisionManager = collisionManager;
    }

    public static CollisionInitializer getCollisionInitializer() {
        return collisionInitializer;
    }

    public static void setCollisionInitializer(final CollisionInitializer collisionInitializer) {
        ProjectKorra.collisionInitializer = collisionInitializer;
    }

    public static boolean isStatisticsEnabled() {
        return ConfigManager.getConfig(null).getBoolean("Properties.Statistics");
    }

    public static boolean isDatabaseCooldownsEnabled() {
        return ConfigManager.getConfig(null).getBoolean("Properties.DatabaseCooldowns");
    }

    @Override
    public void onLoad() {
        if (Bukkit.getPluginManager().getPlugin("WorldGuard") != null) {
            FlagRegistry registry = WorldGuardPlugin.inst().getFlagRegistry();
            registry.register(BENDING);
            registry.register(AVATAR_BENDING);
        }
    }

    @Override
    public void onEnable() {
        plugin = this;
        ProjectKorra.log = this.getLogger();

        new ConfigManager();
        new GeneralMethods(this);
        new Commands(this);
        new MultiAbilityManager();
        new ComboManager();
        collisionManager = new CollisionManager();
        collisionInitializer = new CollisionInitializer(collisionManager);
        CoreAbility.registerAbilities();
        collisionInitializer.initializeDefaultCollisions();
        collisionManager.startCollisionDetection();

        Preset.loadExternalPresets();

        DBConnection.init();
        if (!DBConnection.isOpen()) {
            return;
        }

        Manager.startup();

        this.getServer().getPluginManager().registerEvents(new PKListener(this), this);
        this.getServer().getScheduler().scheduleSyncRepeatingTask(this, new BendingManager(), 0, 1);
        this.getServer().getScheduler().scheduleSyncRepeatingTask(this, new FlightController(), 0, 1);
        this.getServer().getScheduler().scheduleSyncRepeatingTask(this, new MovePreviewManager(), 0, 20);
        this.getServer().getScheduler().scheduleSyncRepeatingTask(this, new AirbendingManager(this), 0, 1);
        this.getServer().getScheduler().scheduleSyncRepeatingTask(this, new WaterbendingManager(this), 0, 1);
        this.getServer().getScheduler().scheduleSyncRepeatingTask(this, new EarthbendingManager(this), 0, 1);
        this.getServer().getScheduler().scheduleSyncRepeatingTask(this, new FirebendingManager(this), 0, 1);
        this.getServer().getScheduler().scheduleSyncRepeatingTask(this, new ChiblockingManager(this), 0, 1);
        this.getServer().getScheduler().runTaskTimerAsynchronously(this, new RevertChecker(this), 0, 200);
        TempBlock.startReversion();

        for (final Player player : Bukkit.getOnlinePlayers()) {
            PKListener.getJumpStatistics().put(player, player.getStatistic(Statistic.JUMP));

            GeneralMethods.createBendingPlayer(player.getUniqueId(), player.getName());
            GeneralMethods.removeUnusableAbilities(player.getName());
            Manager.getManager(StatisticsManager.class).load(player.getUniqueId());
            Bukkit.getScheduler().runTaskLater(ProjectKorra.plugin, new Runnable() {
                @Override
                public void run() {
                    PassiveManager.registerPassives(player);
                    GeneralMethods.removeUnusableAbilities(player.getName());
                }
            }, 5);
        }

        final double cacheTime = ConfigManager.getConfig(null).getDouble("Properties.RegionProtection.CacheBlockTime");
        GeneralMethods.deserializeFile();
        GeneralMethods.startCacheCleaner(cacheTime);
    }

    @Override
    public void onDisable() {
        GeneralMethods.stopBending();
        for (final Player player : this.getServer().getOnlinePlayers()) {
            if (isStatisticsEnabled()) {
                Manager.getManager(StatisticsManager.class).save(player.getUniqueId(), false);
            }
            final BendingPlayer bPlayer = BendingPlayer.getBendingPlayer(player);
            if (bPlayer != null && isDatabaseCooldownsEnabled()) {
                bPlayer.saveCooldowns();
            }
        }
        Manager.shutdown();
        if (DBConnection.isOpen()) {
            DBConnection.sql.close();
        }
    }

}
