package com.projectkorra.projectkorra.chiblocking;

import com.projectkorra.projectkorra.GeneralMethods;
import com.projectkorra.projectkorra.ability.ChiAbility;
import com.projectkorra.projectkorra.configuration.ConfigManager;
import org.bukkit.Location;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

import java.util.concurrent.ConcurrentHashMap;

public class Lasso extends ChiAbility {

    private ConcurrentHashMap<Location, Double> locations = new ConcurrentHashMap();
    private long cooldown;
    private long threshold;
    private double range;
    private double speed;
    private boolean controllable;
    private int shots;
    private long lastShotTime;
    private Vector direction;

    public Lasso(Player player) {
        super(player);
        if (!this.bPlayer.canBend(this)) {
            return;
        }
        if (hasAbility(player, Lasso.class)) {
            Lasso as = (Lasso) getAbility(player, Lasso.class);
            if (as != null)
                as.createShot();
            return;
        }
        setFields();
        this.direction = player.getEyeLocation().getDirection();

        start();
        createShot();
    }

    private void setFields() {
        this.cooldown = getConfig().getLong("Abilities.Chi.Lasso.Cooldown");
        this.range = getConfig().getDouble("Abilities.Chi.Lasso.Range");
        this.speed = getConfig().getDouble("Abilities.Chi.Lasso.Speed");
        this.controllable = true;

        this.threshold = 500L;
        this.shots = 7;
    }

    private void createShot() {
        if (this.shots >= 7) {
            this.lastShotTime = System.currentTimeMillis();
            this.shots -= 1;
            this.locations.put(this.player.getEyeLocation().add(this.player.getLocation().getDirection().multiply(1.5D).normalize()), Double.valueOf(0.0D));
        }
    }

    public long getCooldown() {
        return this.cooldown;
    }

    public Location getLocation() {
        return null;
    }

    public String getName() {
        return "Lasso";
    }

    public boolean isHarmlessAbility() {
        return false;
    }

    public boolean isSneakAbility() {
        return false;
    }

    public void progress() {
        progressSlashes();
        if ((this.player.isDead()) || (!this.player.isOnline())) {
            remove();
            return;
        }
        if (!this.bPlayer.canBendIgnoreBindsCooldowns(this)) {
            prepareRemove();
            return;
        }
        if ((this.shots == 7) || (System.currentTimeMillis() > this.lastShotTime + this.threshold)) {
            prepareRemove();
        }
    }

    private void prepareRemove() {
        if ((this.player.isOnline()) && (!this.bPlayer.isOnCooldown(this))) {
            this.bPlayer.addCooldown(this);
        }
        if (this.locations.isEmpty()) {
            remove();
        }
    }

    private void progressSlashes() {
        for (Location l : this.locations.keySet()) {
            Location loc = l.clone();
            double dist = this.locations.get(l);
            boolean cancel = false;
            for (int i = 0; i < 3; i++) {
                dist += 1.0D;
                if ((cancel) || (dist >= this.range)) {
                    cancel = true;
                    break;
                }
                if (this.controllable) {
                    this.direction = this.player.getLocation().getDirection();
                }
                loc = loc.add(this.direction.clone().multiply(1));
                if ((GeneralMethods.isSolid(loc.getBlock())) || (GeneralMethods.isRegionProtectedFromBuild(this.player, "Lasso", loc))) {
                    cancel = true;
                    break;
                }
                GeneralMethods.displayColoredParticle(loc, "442200", 0F, 0F, 0F);
                for (Entity entity : GeneralMethods.getEntitiesAroundPoint(loc, 2.5D)) {
                    if (((entity instanceof LivingEntity)) && (entity.getEntityId() != this.player.getEntityId()) && (!(entity instanceof ArmorStand))) {
                        bleed(entity);
                        remove();
                        return;
                    }
                }
            }
            if (cancel) {
                this.locations.remove(l);
            } else {
                this.locations.remove(l);
                this.locations.put(loc, dist);
            }
        }
    }

    private void bleed(Entity entity) {
        Vector vec = new Vector(player.getLocation().getDirection().getX(), player.getLocation().getDirection().getY(), player.getLocation().getDirection().getZ());
        entity.setVelocity(vec.multiply(-speed));
        prepareRemove();
        remove();
    }
}
