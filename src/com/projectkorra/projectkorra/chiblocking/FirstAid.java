package com.projectkorra.projectkorra.chiblocking;

import com.projectkorra.projectkorra.ability.ChiAbility;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class FirstAid extends ChiAbility {

    private long cooldown;
    private int power;

    public FirstAid(Player player) {
        super(player);
        this.cooldown = getConfig().getLong("Abilities.Chi.FirstAid.Cooldown");
        this.power = getConfig().getInt("Abilities.Chi.FirstAid.Power");

        start();
    }

    @Override
    public void progress() {
        if (player.getEyeLocation().getBlock().isLiquid()) {
            remove();
            return;
        } else if (!bPlayer.canBendIgnoreBinds(this)) {
            remove();
            return;
        }
        if (!player.isSneaking()) {
            bPlayer.addCooldown(this);
            remove();
            return;
        }
        if (player.getHealth() < player.getMaxHealth()) {
            heal();
        } else {
            bPlayer.addCooldown(this);
            remove();
            return;
        }
    }

    private void heal() {
        if (!player.hasPotionEffect(PotionEffectType.REGENERATION)) {
            player.addPotionEffect(new PotionEffect(PotionEffectType.REGENERATION, 20, power), true);
        }
    }

    @Override
    public String getName() {
        return "FirstAid";
    }

    @Override
    public Location getLocation() {
        return player != null ? player.getLocation() : null;
    }

    @Override
    public long getCooldown() {
        return cooldown;
    }

    @Override
    public boolean isSneakAbility() {
        return true;
    }

    @Override
    public boolean isHarmlessAbility() {
        return false;
    }
}