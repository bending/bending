package com.projectkorra.projectkorra.chiblocking;

import com.projectkorra.projectkorra.BendingPlayer;
import com.projectkorra.projectkorra.GeneralMethods;
import com.projectkorra.projectkorra.ability.ChiAbility;
import com.projectkorra.projectkorra.ability.CoreAbility;
import com.projectkorra.projectkorra.configuration.ConfigManager;
import org.bukkit.Location;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.util.Vector;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

public class Bolas extends ChiAbility {
    private ConcurrentHashMap<Location, Double> locations = new ConcurrentHashMap();
    private long cooldown;
    private long threshold;
    private long lastShotTime;
    private long time;
    private double range;
    private DiscRenderer discRenderer;
    private int duration;
    private boolean controllable;
    private boolean effect = false;
    private int shots;
    private Vector direction;
    private LivingEntity lee;

    public Bolas(Player player) {
        super(player);
        if (!this.bPlayer.canBend(this)) {
            return;
        }
        if (hasAbility(player, Bolas.class)) {
            Bolas as = (Bolas) getAbility(player, Bolas.class);
            as.createShot();
            return;
        }
        setFields();
        this.direction = player.getEyeLocation().getDirection();

        start();
        createShot();
    }

    public static List<Location> getCirclePoints(Location location, int points, double size) {
        return getCirclePoints(location, points, size, 0);
    }

    public static List<Location> getCirclePoints(Location location, int points, double size, double startangle) {
        List<Location> locations = new ArrayList<Location>();
        for (int i = 0; i < 360; i += 360 / points) {
            double angle = (i * Math.PI / 180);
            double x = size * Math.cos(angle + startangle);
            double z = size * Math.sin(angle + startangle);
            Location loc = location.clone();
            loc.add(x, 0, z);
            locations.add(loc);
        }
        return locations;
    }

    private void setFields() {
        this.cooldown = getConfig().getLong("Abilities.Chi.Bolas.Cooldown");
        this.range = getConfig().getDouble("Abilities.Chi.Bolas.Range");
        this.duration = getConfig().getInt("Abilities.Chi.Bolas.Duration") / 50;
        this.controllable = true;

        this.threshold = 500L;
        this.shots = 7;
        discRenderer = new DiscRenderer(this.player);
    }

    private void createShot() {
        if (this.shots >= 7) {
            this.lastShotTime = System.currentTimeMillis();
            this.shots -= 1;
            this.locations.put(this.player.getEyeLocation().add(this.player.getLocation().getDirection().multiply(1.5D).normalize()), Double.valueOf(0.0D));
        }
    }

    public long getCooldown() {
        return this.cooldown;
    }

    public Location getLocation() {
        return null;
    }

    public String getName() {
        return "Bolas";
    }

    public boolean isHarmlessAbility() {
        return false;
    }

    public boolean isSneakAbility() {
        return false;
    }

    public void progress() {
        progressSlashes();
        if ((this.player.isDead()) || (!this.player.isOnline())) {
            remove();
            return;
        }
        if (!this.bPlayer.canBendIgnoreBindsCooldowns(this)) {
            prepareRemove();
            return;
        }
        if (!effect) {
            if ((this.shots == 7) || (System.currentTimeMillis() > this.lastShotTime + this.threshold)) {
                prepareRemove();
                return;
            }
        }
        if (effect) {
            if (System.currentTimeMillis() < time) {
                render(lee.getLocation().clone().add(0, 0.3, 0), 0);
            } else {
                prepareRemove();
                remove();
                return;
            }
        }
    }

    private void prepareRemove() {
        if ((this.player.isOnline()) && (!this.bPlayer.isOnCooldown(this))) {
            this.bPlayer.addCooldown(this);
        }
        if (this.locations.isEmpty()) {
            remove();
            return;
        }
    }

    private void progressSlashes() {
        for (Location l : this.locations.keySet()) {
            Location loc = l.clone();
            double dist = this.locations.get(l);
            boolean cancel = false;
            for (int i = 0; i < 3; i++) {
                dist += 1.0D;
                if ((cancel) || (dist >= this.range)) {
                    cancel = true;
                    break;
                }
                if (this.controllable) {
                    this.direction = this.player.getLocation().getDirection();
                }
                loc = loc.add(this.direction.clone().multiply(1));
                if ((GeneralMethods.isSolid(loc.getBlock())) || (GeneralMethods.isRegionProtectedFromBuild(this.player, "Bolas", loc))) {
                    cancel = true;
                    break;
                }
                if (!effect)
                    discRenderer.render(loc);
                for (Entity entity : GeneralMethods.getEntitiesAroundPoint(loc, 2.5D)) {
                    if (((entity instanceof LivingEntity)) && (entity.getEntityId() != this.player.getEntityId()) && (!(entity instanceof ArmorStand))) {
                        bleed(entity);
                        time = System.currentTimeMillis() + duration * 50;
                    }
                }
            }
            if (cancel) {
                this.locations.remove(l);
            } else {
                this.locations.remove(l);
                this.locations.put(loc, Double.valueOf(dist));
            }
        }
    }

    private void render(Location location, int angle) {
        GeneralMethods.displayColoredParticle(location, "000000", 0F, 0F, 0F);
        angle += 1;
        if (angle > 360)
            angle = 0;

        for (Location l : getCirclePoints(location, 10, 0.25, angle)) {
            GeneralMethods.displayColoredParticle(l, "000000", 0F, 0F, 0F);
            GeneralMethods.displayColoredParticle(l, "000000", 0F, 0F, 0F);
        }
    }

    public void bleed(Entity entity) {
        LivingEntity le = (LivingEntity) entity;
        if (!effect) {
            effect = true;
            lee = le;
            le.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, duration, 255), true);
        }
    }

    private class DiscRenderer {
        private int angle;

        public DiscRenderer(Player player) {
            this.angle = 0;
        }

        private void render(Location location) {
            GeneralMethods.displayColoredParticle(location, "000000", 0F, 0F, 0F);
            angle += 1;
            if (angle > 360)
                angle = 0;

            for (Location l : getCirclePoints(location, 20, 1, angle)) {
                GeneralMethods.displayColoredParticle(l, "000000", 0F, 0F, 0F);
            }

            for (Location l : getCirclePoints(location, 10, 0.5, angle)) {
                GeneralMethods.displayColoredParticle(l, "000000", 0F, 0F, 0F);
                GeneralMethods.displayColoredParticle(l, "000000", 0F, 0F, 0F);
            }
        }

    }

}
