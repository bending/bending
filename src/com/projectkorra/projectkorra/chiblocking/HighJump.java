package com.projectkorra.projectkorra.chiblocking;

import com.projectkorra.projectkorra.ability.ChiAbility;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

public class HighJump extends ChiAbility {

    public enum HighJumpType {
        EVADE, LUNGE, DOUBLEJUMP, JUMP
    }
    private HighJumpType highJumpType;

    private long jumpCooldown;
    private long jumpHeight;
    private long lungeCooldown;
    private long lungeHeight;
    private long evadeCooldown;
    private long evadeHeight;
    private long doubleJumpCooldown;
    private long doubleJumpHeight;
    private long evadeDistance;
    private long lungeDistance;

    private Location location;

    private boolean enableLunge;
    private boolean enableJump;
    private boolean enableDoubleJump;
    private boolean enableEvade;

    public HighJump(Player player, HighJumpType highJumpType) {
        super(player);
        if (!bPlayer.canBend(this)) {
            return;
        }
        this.highJumpType = highJumpType;
        setFields();
        start();
    }

    private void setFields() {
        this.enableEvade = getConfig().getBoolean("Abilities.HighJump.Evade.Enabled");
        this.enableJump = getConfig().getBoolean("Abilities.HighJump.Jump.Enabled");
        this.enableDoubleJump = getConfig().getBoolean("Abilities.HighJump.DoubleJump.Enabled");
        this.enableLunge = getConfig().getBoolean("Abilities.HighJump.Lunge.Enabled");

        this.jumpCooldown = getConfig().getLong("Abilities.HighJump.Jump.Cooldown");
        this.jumpHeight = getConfig().getLong("Abilities.HighJump.Jump.Height");

        this.doubleJumpCooldown = getConfig().getLong("Abilities.HighJump.DoubleJump.Cooldown");
        this.doubleJumpHeight = getConfig().getLong("Abilities.HighJump.DoubleJump.Height");

        this.lungeCooldown = getConfig().getLong("Abilities.HighJump.Lunge.Cooldown");
        this.lungeHeight = getConfig().getLong("Abilities.HighJump.Lunge.Height");
        this.lungeDistance = getConfig().getLong("Abilities.HighJump.Lunge.Distance");

        this.evadeCooldown = getConfig().getLong("Abilities.HighJump.Evade.Cooldown");
        this.evadeHeight = getConfig().getLong("Abilities.HighJump.Evade.Height");
        this.evadeDistance = getConfig().getLong("Abilities.HighJump.Evade.Distance");

        this.location = player.getLocation().clone();
    }

    @Override
    public void progress() {
        if (player.isDead() || !player.isOnline()) {
            remove();
            return;
        }

        switch (this.highJumpType) {
            case DOUBLEJUMP: this.onDoubleJump();
            case EVADE: this.onEvade();
            case JUMP: this.onJump();
            case LUNGE: this.onLunge();
        }
    }
    private void onDoubleJump() {
        if (enableDoubleJump && this.highJumpType == HighJumpType.DOUBLEJUMP) {
            Vector vec = player.getVelocity();
            vec.setY(doubleJumpHeight);
            player.setVelocity(vec);
            bPlayer.addCooldown(this, doubleJumpCooldown);
            remove();
        }
    }
    private void onEvade() {
        if (enableEvade && this.highJumpType == HighJumpType.EVADE) {
            Vector vec = player.getLocation().getDirection().normalize().multiply(-evadeDistance);
            vec.setY(evadeHeight);
            player.setVelocity(vec);
            bPlayer.addCooldown(this, evadeCooldown);
            remove();
        }
    }
    private void onJump() {
        if (enableJump && this.highJumpType == HighJumpType.JUMP) {
            Vector vec = player.getVelocity();
            vec.setY(jumpHeight);
            player.setVelocity(vec);
            bPlayer.addCooldown(this, jumpCooldown);
            remove();
        }
    }
    private void onLunge() {
        if (enableLunge && this.highJumpType == HighJumpType.LUNGE) {
            Vector vec = player.getLocation().getDirection().normalize().multiply(lungeDistance);
            vec.setY(lungeHeight);
            player.setVelocity(vec);
            bPlayer.addCooldown(this, lungeCooldown);
            remove();
        }
    }

    @Override
    public boolean isSneakAbility() {
        return true;
    }

    @Override
    public boolean isHarmlessAbility() {
        return true;
    }

    @Override
    public long getCooldown() {
        switch (this.highJumpType) {
            case JUMP: return jumpCooldown;
            case EVADE: return evadeCooldown;
            case LUNGE: return lungeCooldown;
            case DOUBLEJUMP: return doubleJumpCooldown;
            default: return 0;
        }
    }

    @Override
    public String getName() {
        return "HighJump";
    }

    @Override
    public Location getLocation() {
        return location;
    }
}