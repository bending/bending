package com.projectkorra.projectkorra.chiblocking;

import com.projectkorra.projectkorra.ability.ChiAbility;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;


public class ShadowSuit extends ChiAbility {
    private long duration;
    private long cooldown;

    public ShadowSuit(Player player) {
        super(player);
        if (!this.bPlayer.canBend(this)) {
            return;
        }
        this.duration = getConfig().getLong("Abilities.Chi.ShadowSuit.Duration");
        this.cooldown = getConfig().getLong("Abilities.Chi.ShadowSuit.Cooldown");
        this.start();
    }

    public void progress() {
        if (!this.bPlayer.isOnCooldown(this) && !player.hasPotionEffect(PotionEffectType.INVISIBILITY)) {
            player.addPotionEffect(new PotionEffect(PotionEffectType.INVISIBILITY, ((int) duration / 1000) * 20, 1));
            this.bPlayer.addCooldown(this);
        }
        this.remove();
    }


    public String getName() {
        return "ShadowSuit";
    }

    public Location getLocation() {
        return player != null ? player.getLocation() : null;
    }

    public long getCooldown() {
        return cooldown;
    }

    public boolean isSneakAbility() {
        return false;
    }

    public boolean isHarmlessAbility() {
        return true;
    }

}
