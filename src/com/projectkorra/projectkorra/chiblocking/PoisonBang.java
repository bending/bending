package com.projectkorra.projectkorra.chiblocking;

import com.projectkorra.projectkorra.GeneralMethods;
import com.projectkorra.projectkorra.ability.ChiAbility;
import com.projectkorra.projectkorra.command.Commands;
import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Snowball;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class PoisonBang extends ChiAbility {

    private static final Map<Integer, PoisonBang> SNOWBALLS = new ConcurrentHashMap<>();
    private static final Map<String, Long> BLINDED_TIMES = new ConcurrentHashMap<>();
    private static final Map<String, PoisonBang> BLINDED_TO_ABILITY = new ConcurrentHashMap<>();

    private int duration;
    private long cooldown;
    private double radius;

    public PoisonBang(Player player) {
        super(player);
        if (!bPlayer.canBend(this)) {
            return;
        }
        this.cooldown = getConfig().getLong("Abilities.Chi.PoisonBang.Cooldown");
        this.duration = getConfig().getInt("Abilities.Chi.PoisonBang.Duration");
        this.radius = getConfig().getDouble("Abilities.Chi.PoisonBang.Radius");
        start();
    }

    public static void playEffect(Location loc) {
        int z = -2;
        int x = -2;
        int y = 0;

        for (int i = 0; i < 125; i++) {
            Location newLoc = new Location(loc.getWorld(), loc.getX() + x, loc.getY() + y, loc.getZ() + z);
            for (int direction = 0; direction < 8; direction++) {
                loc.getWorld().playEffect(newLoc, Effect.SMOKE, direction);
            }
            if (z == 2) {
                z = -2;
            }
            if (x == 2) {
                x = -2;
                z++;
            }
            x++;
        }
    }

    public static void removeFromHashMap(Entity entity) {
        if (entity instanceof Player) {
            Player p = (Player) entity;
            if (BLINDED_TIMES.containsKey(p.getName())) {
                PoisonBang PoisonBang = BLINDED_TO_ABILITY.get(p.getName());
                if (BLINDED_TIMES.get(p.getName()) + PoisonBang.duration >= System.currentTimeMillis()) {
                    BLINDED_TIMES.remove(p.getName());
                    BLINDED_TO_ABILITY.remove(p.getName());
                }
            }
        }
    }

    public static Map<Integer, PoisonBang> getSnowballs() {
        return SNOWBALLS;
    }

    @Override
    public void progress() {
        SNOWBALLS.put(player.launchProjectile(Snowball.class).getEntityId(), this);
        bPlayer.addCooldown(this);
        remove();
    }

    public void applyBlindness(Entity entity) {
        if (entity instanceof Player) {
            if (Commands.invincible.contains(((Player) entity).getName())) {
                return;
            } else if (GeneralMethods.isRegionProtectedFromBuild(this, entity.getLocation())) {
                return;
            }
            Player p = (Player) entity;
            p.addPotionEffect(new PotionEffect(PotionEffectType.POISON, duration * 20, 2));
            BLINDED_TIMES.put(p.getName(), System.currentTimeMillis());
            BLINDED_TO_ABILITY.put(p.getName(), this);
        }
    }

    @Override
    public String getName() {
        return "PoisonBang";
    }

    @Override
    public Location getLocation() {
        return player != null ? player.getLocation() : null;
    }

    @Override
    public long getCooldown() {
        return cooldown;
    }

    @Override
    public boolean isSneakAbility() {
        return false;
    }

    @Override
    public boolean isHarmlessAbility() {
        return false;
    }

    public double getRadius() {
        return radius;
    }
}