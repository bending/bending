package com.projectkorra.projectkorra;

import com.projectkorra.projectkorra.ability.CoreAbility;
import com.projectkorra.projectkorra.ability.ElementalAbility;
import com.projectkorra.projectkorra.configuration.ConfigManager;
import com.projectkorra.projectkorra.earthbending.metal.MetalClips;
import com.projectkorra.projectkorra.object.HorizontalVelocityTracker;
import com.projectkorra.projectkorra.util.ActionBar;
import com.projectkorra.projectkorra.util.RevertChecker;
import com.projectkorra.projectkorra.util.TempArmor;
import com.projectkorra.projectkorra.util.TempPotionEffect;
import com.projectkorra.projectkorra.waterbending.blood.Bloodbending;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.World;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.UUID;

public class BendingManager implements Runnable {

    public static HashMap<World, String> events = new HashMap<World, String>(); // holds any current event.
    private static BendingManager instance;
    private final HashMap<World, Boolean> times = new HashMap<World, Boolean>(); // true if day time
    long time;
    long interval;

    public BendingManager() {
        instance = this;
        this.time = System.currentTimeMillis();
    }

    public static BendingManager getInstance() {
        return instance;
    }


    public void handleCooldowns() {
        for (final UUID uuid : BendingPlayer.getPlayers().keySet()) {
            final BendingPlayer bPlayer = BendingPlayer.getPlayers().get(uuid);
            for (final String abil : bPlayer.getCooldowns().keySet()) {
                if (System.currentTimeMillis() >= bPlayer.getCooldown(abil)) {
                    bPlayer.removeCooldown(abil);
                }
            }
        }
    }

    @Override
    public void run() {
        try {
            this.interval = System.currentTimeMillis() - this.time;
            this.time = System.currentTimeMillis();
            ProjectKorra.time_step = this.interval;

            CoreAbility.progressAll();
            TempPotionEffect.progressAll();
            RevertChecker.revertAirBlocks();
            HorizontalVelocityTracker.updateAll();
            this.handleCooldowns();
            TempArmor.cleanup();

            for (final Player player : Bukkit.getOnlinePlayers()) {
                if (Bloodbending.isBloodbent(player)) {
                    ActionBar.sendActionBar(Element.BLOOD.getColor() + "* Подконтролен магом крови *", player);
                } else if (MetalClips.isControlled(player)) {
                    ActionBar.sendActionBar(Element.METAL.getColor() + "* Зажат в метале *", player);
                }
            }
        } catch (final Exception e) {
            e.printStackTrace();
        }
    }

}
