package com.projectkorra.projectkorra.util;

import com.projectkorra.projectkorra.BendingPlayer;
import com.projectkorra.projectkorra.GeneralMethods;
import com.projectkorra.projectkorra.event.MoveCoolDownTickEvent;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public class MovePreviewManager implements Runnable {

    @Override
    public void run() {
        for (Player player : Bukkit.getOnlinePlayers()) {
            final BendingPlayer bPlayer = BendingPlayer.getBendingPlayer(player);
            if (bPlayer == null)
                continue;
            final MoveCoolDownTickEvent event = new MoveCoolDownTickEvent(bPlayer);
            Bukkit.getServer().getPluginManager().callEvent(event);
            if (!event.isCancelled())
                GeneralMethods.displayMovePreview(player);
        }
    }

}
