package com.projectkorra.projectkorra.util;

import com.projectkorra.projectkorra.BendingPlayer;
import com.projectkorra.projectkorra.Element;
import com.projectkorra.projectkorra.GeneralMethods;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

public class ElementController extends BukkitRunnable {

    private Player player;

    public ElementController(Player player) {
        this.player = player;
    }

    @Override
    public void run() {
        BendingPlayer bp = BendingPlayer.getBendingPlayer(player.getName());

        for (Element element : Element.getAllElements()) {
            if (player.hasPermission("bending." + element.getName())) {
                if (!bp.hasElement(element)) {
                    bp.addElement(element);
                }
            } else {
                if (bp.hasElement(element)) {
                    bp.getElements().remove(element);
                }
            }
        }

        for (Element.SubElement element : Element.getAllSubElements()) {
            if (player.hasPermission("bending." + element.getParentElement().getName() + "." + element.getName())) {
                if (!bp.hasSubElement(element)) {
                    bp.addSubElement(element);
                }
            } else {
                if (bp.hasSubElement(element)) {
                    bp.getSubElements().remove(element);
                }
            }
        }

        GeneralMethods.saveElements(bp);
        GeneralMethods.saveSubElements(bp);
    }
}
