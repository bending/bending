package com.projectkorra.projectkorra.util;

import com.projectkorra.projectkorra.ability.CoreAbility;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;

public class FlightController implements Runnable {

    @Override
    public void run() {
        for (Player p : Bukkit.getOnlinePlayers())
            if (p.isFlying() || p.getAllowFlight())
                controlIllegalFlight(p);
    }

    private void controlIllegalFlight(Player player) {
        GameMode gm = player.getGameMode();
        if (!player.hasPermission("essentials.fly") && (gm == GameMode.SURVIVAL || gm == GameMode.ADVENTURE)) {
            for (CoreAbility ca : CoreAbility.getAbilitiesByInstances())
                if (ca.isFlight() && ca.getPlayer() == player) {
                    return;
                }
            if (player.isFlying())
                player.setFlying(false);
            if (player.getAllowFlight())
                player.setAllowFlight(false);
        }

    }

}
