package com.projectkorra.projectkorra.waterbending.passive;

import com.projectkorra.projectkorra.ability.CoreAbility;
import com.projectkorra.projectkorra.ability.ElementalAbility;
import com.projectkorra.projectkorra.ability.PassiveAbility;
import com.projectkorra.projectkorra.ability.WaterAbility;
import com.projectkorra.projectkorra.configuration.ConfigManager;
import com.projectkorra.projectkorra.earthbending.EarthArmor;
import com.projectkorra.projectkorra.waterbending.WaterSpout;
import com.projectkorra.projectkorra.waterbending.multiabilities.WaterArms;
import org.bukkit.Location;
import org.bukkit.entity.Player;

public class FastSwim extends WaterAbility implements PassiveAbility {
    private long cooldown;
    private double swimSpeed;
    private long duration;
    private Boolean previousGlidingState;
    private Boolean showGliding;
    private Boolean toggleGliding = false;

    public FastSwim(final Player player) {
        super(player);
        if (this.bPlayer.isOnCooldown(this)) {
            return;
        }

        this.cooldown = getConfig().getLong("Abilities.Water.Passive.FastSwim.Cooldown");
        this.swimSpeed = getConfig().getDouble("Abilities.Water.Passive.FastSwim.SpeedFactor");
        this.duration = getConfig().getLong("Abilities.Water.Passive.FastSwim.Duration");
        this.showGliding = getConfig().getBoolean("Abilities.Water.Passive.FastSwim.ShowGliding");
    }

    public static double getSwimSpeed() {
        return ConfigManager.getConfig(null).getDouble("Abilities.Water.Passive.FastSwim.SpeedFactor");
    }

    @Override
    public void progress() {
        if (!this.bPlayer.canUsePassive(this) || !this.bPlayer.canBendPassive(this) || CoreAbility.hasAbility(this.player, WaterSpout.class) || CoreAbility.hasAbility(this.player, EarthArmor.class) || CoreAbility.hasAbility(this.player, WaterArms.class)) {
            return;
        }

        if (this.bPlayer.getBoundAbility() == null || (this.bPlayer.getBoundAbility() != null && !this.bPlayer.getBoundAbility().isSneakAbility())) {
            if (this.player.isSneaking() && ElementalAbility.isWater(this.player.getLocation().getBlock()) && !this.bPlayer.isOnCooldown(this)) {
                if (this.duration != 0 && System.currentTimeMillis() > this.getStartTime() + this.duration) {
                    this.bPlayer.addCooldown(this);
                    return;
                }
                if (showGliding) {
                    previousGlidingState = player.isGliding();
                    player.setGliding(true);
                    toggleGliding = true;
                }
                this.player.setVelocity(this.player.getEyeLocation().getDirection().clone().normalize().multiply(this.swimSpeed));
            } else if (toggleGliding) {
                player.setGliding(previousGlidingState);
                toggleGliding = false;
            } else if (!this.player.isSneaking()) {
                this.bPlayer.addCooldown(this);
            }
        }
    }

    @Override
    public boolean isSneakAbility() {
        return true;
    }

    @Override
    public boolean isHarmlessAbility() {
        return true;
    }

    @Override
    public long getCooldown() {
        return this.cooldown;
    }

    @Override
    public String getName() {
        return "FastSwim";
    }

    @Override
    public Location getLocation() {
        return this.player.getLocation();
    }

    @Override
    public boolean isInstantiable() {
        return true;
    }

    @Override
    public boolean isProgressable() {
        return true;
    }
}
