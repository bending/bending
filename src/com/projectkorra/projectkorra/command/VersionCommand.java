package com.projectkorra.projectkorra.command;

import com.projectkorra.projectkorra.GeneralMethods;
import com.projectkorra.projectkorra.ProjectKorra;
import com.projectkorra.projectkorra.configuration.ConfigManager;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

import java.util.List;

/**
 * Executor for /bending version. Extends {@link PKCommand}.
 */
public class VersionCommand extends PKCommand {

    public VersionCommand() {
        super("version", "/bending version", ConfigManager.languageConfig.get().getString("Commands.Version.Description"), new String[]{"version", "v"});
    }

    @Override
    public void execute(final CommandSender sender, final List<String> args) {
        if (!this.hasPermission(sender) || !this.correctLength(sender, args.size(), 0, 0)) {
            return;
        }

        sender.sendMessage(ChatColor.GREEN + "Версия: " + ChatColor.RED + ProjectKorra.plugin.getDescription().getVersion());
        sender.sendMessage(ChatColor.GREEN + "Разработал " + ChatColor.RED + "CKATEPTb");
        sender.sendMessage(ChatColor.GREEN + "Специально для проекта " + ChatColor.RED + "www.bd-server.com " + ChatColor.GREEN + ".");
    }

}
